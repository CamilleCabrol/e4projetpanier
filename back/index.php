<?php
session_start();
if(isset($_GET['action'])) {
    $action=$_GET["action"];
    if($action == "deconnexion") {
        session_destroy();
        header("Location:index.php");
    }
}
?>

<!DOCTYPE html>
<html>

    <head>
        <title>BonbonTropBon BackOffice</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="images/logo.jpg" type="image/x-icon" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" data-auto-replace-svg="nest"></script>
    </head>

    <body>
        <?php
        if (!isset($_SESSION['admin'])) {
        ?>
        <div class="container">
            <br/>
            <?php
                include "vues/connexion.php";
            ?>
        </div> 
        <?php
        } else {
            include "vues/include/header.inc.php";
        ?>
            <div>
            <?php
                include "vues/include/nav.inc.php";
            ?>
            <div class="container">
                <br/>
                <?php
                    if (!isset($_GET['page'])) {
                        include "vues/include/index.inc.php";
                    } else {
                        $page = $_GET["page"];
                        if (file_exists("controleur/$page.php")) {
                            include "controleur/$page.php";
                        } else {
                            include "vues/include/error.inc.php";
                        }
                    }
        }
                ?>
            </div>
        </div>
        <?php
            include "vues/include/footer.inc.php";
        ?>
    </body>

</html>