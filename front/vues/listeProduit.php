<link rel="stylesheet" href="style.css">
<br/>
<h2>Nos suggestions</h2>
<span><i>Un produit correspond à 100g de bonbons.</i></span>
<br /><br/>
<?php
foreach ($lesProduits as $unProduit) {
?>
    <div class="produits">
        <div class="img-prod">
            <?php echo "<img src='images/" . $unProduit->image . "'>"; ?>
        </div>
        <div class="desc-prod">
            <div class="nom-prod">
                <?php echo $unProduit->nom; ?>
            </div>
            <div class="prix-prod">
                <?php
                if ($unProduit->prix == 1 || $unProduit->prix == 2) {
                    echo $unProduit->prix . ".00 €";
                } else {
                    echo $unProduit->prix . " €";
                }
                ?>
            </div>
        </div>
        <div class="btn-prod">
            <!--<button type="submit" class="btn btn-danger">Ajouter au panier</button>-->
            <a class="btn btn-danger" href='controleur/actionPanier.php?action=1&numProduit=<?php echo $unProduit->num; ?>'>Ajouter au panier</a>
        </div>
    </div>
<?php }
?>