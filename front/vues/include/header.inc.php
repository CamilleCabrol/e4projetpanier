<!--<p class="header-txt"><span style="color:white;font-size:15px;"> Bienvenue chez <b>BonbonTropBon</b>® ! ❤ </span></p>

<div class="header-img">
    <img src="images/fondheader.jpg" height="317">
</div>-->

<div class="header-banner slide-up" style="background-color: #ff6633; text-align: center; color: #fff; padding-top: 0.625 rem; padding-right: 0px; padding-bottom: 0.625rem; padding-left: 0px; ">
    <div class="container" style="max-width: 1140px;">
        <div class="header-promotion-container" style="display: flex; justify-content: center; position: relative;">
            <div class="header-promotion">
                <div class="html-slot-container" style="white-space: nowrap; overflow-x: hidden; overflow-y: hidden; text-overflow: ellipsis;">
                    <div class="header-promotion">
                        Bienvenue chez <strong>BonbonTropBon</strong>© ! ❤
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
    <div class="carousel-inner">
        <div class="carousel-item active">
            <img src="images/img1-header.jpg" class="d-block w-100" >
        </div>
        <div class="carousel-item">
          <img src="images/img2-header.jpg" class="d-block w-100" >
        </div>
        <div class="carousel-item">
          <img src="images/img3-header.jpg" class="d-block w-100" >
        </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>