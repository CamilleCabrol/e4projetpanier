<!--<form id="frmSaisie" method="POST" action="controleur/actionClient.php">
    <fieldset>
        <legend>Entrez vos informations</legend>
        <table>
            <!-- adresse mail -->
            <!--<tr>
                <td class="alignDroit"><label for="txtMel">Adresse électronique : </label></td>
                <td class="alignGauche"><input type="text" name="txtMel" id="txtMel"" /><span id="etoileMel" class="messInfo"></span></td>

                <!-- mdp -->
                <!--<td class="alignDroit"><label for="mdp">Mot de passe :</label></td>
                <td class="alignGauche"><input type="password" id="mdp" name="mdp" /></td>
            </tr>
        </table>
    </fieldset>
    <input type="hidden" name="action" value="2"/>
    <input type="submit" name="btEnvoi" value="Se connecter" />

    <?php/*
    if(isset($_GET['erreur'])){
        $err = $_GET['erreur'];
        if($err == 1 || $err == 2){
            echo "<p>Email ou mot de passe incorrect</p>";
        }
    }*/
    ?>
</form>-->

<form id="frmSaisie" method="POST" action="controleur/actionClient.php">
    <div class="form-group">
        <label for="txtMel">Adresse mail</label>
        <input type="email" class="form-control" name="txtMel" id="txtMel" aria-describedby="emailHelp">
        <small id="emailHelp" class="form-text text-muted">Nous ne partagerons jamais votre email avec qui que ce soit.</small>
    </div>
    <div class="form-group">
        <label for="mdp">Mot de passe</label>
        <input type="password" class="form-control" name="mdp" id="mdp">
    </div>
    <input type="hidden" name="action" value="2"/>
    <button type="submit" name="btEnvoi" class="btn btn-danger">Se connecter</button>
    
    <?php
        if(isset($_GET['erreur'])){
            $err = $_GET['erreur'];
            if($err == 1 || $err == 2){
                echo "<p>Email ou mot de passe incorrect</p>";
            }
        }
    ?>
</form>