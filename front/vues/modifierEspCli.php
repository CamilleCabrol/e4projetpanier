<form name="frmInscription" method="POST" action="controleur/actionClient.php">
        <div class="form-group">
        <label for="civilite">Civilité</label>
        <select name="civilite" id="civilite" class="form-control">
            <option value="m" id="m" <?php if($unCli->civilite == "m") { echo 'selected'; } ?>>M.</option>
            <option value="f" id="f" <?php if($unCli->civilite == "f") { echo 'selected'; } ?>>Mme.</option>
        </select>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="txtNom">Nom</label>
            <input type="text" class="form-control" name="txtNom" id="txtNom" value="<?php echo $unCli->nom; ?>">
        </div>
        <div class="form-group col-md-6">
            <label for="txtPrenom">Prénom</label>
            <input type="text" class="form-control" name="txtPrenom" id="txtPrenom" value="<?php echo $unCli->prenom; ?>">
        </div>
    </div>
    <div class="form-group">
        <label for="adresse">Adresse (N°, voie, lieu-dit, ...)</label>
        <input type="text" class="form-control" name ="adresse" id="adresse" value="<?php echo $unCli->ad; ?>">
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="ville">Ville</label>
            <input type="text" class="form-control" name="ville" id="ville" value="<?php echo $unCli->ville; ?>">
        </div>
        <div class="form-group col-md-2">
            <label for="codePostal">Code Postal</label>
            <input type="text" class="form-control" name="codePostal" id="codePostal" maxLength="5" value="<?php echo $unCli->cp; ?>">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="date">Date de naissance</label>
            <input type="date" class="form-control" name="date" id="date" value="<?php echo $unCli->dateNais; ?>">
        </div>
        <div class="form-group col-md-6">
            <label for="txtTel">Numéro de téléphone</label>
            <input type="text" class="form-control" name="txtTel" id="txtTel" maxLength="10" value="<?php echo $unCli->tel; ?>">
        </div>
    </div>

    <input type="hidden" name="action" value="3" />
    <input type="hidden" name="numCli" value=<?php echo $_SESSION['numCli']?> />
    <br />
    <button type="submit" class="btn btn-danger" name="btEnvoi" onclick="window.location.href = 'index.php?page=espaceClient';">Valider</button>
</form>