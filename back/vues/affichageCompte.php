<?php
if (count($CompteListe) == 0) {
?>
    <h2> Aucun compte dans la bdd </h2>
    
<?php
} 
else {
?>
<table class="table table-hover" id="TableauCompte">
    <thead>    
        <tr>
            <th scope="col">Numéro</th>
            <th scope="col">Prénom</th>
            <th scope="col">Nom</th>
            <th scope="col">Civilité</th>
            <th scope="col">Téléphone</th>
            <th scope="col">Date de naissance</th>
            <th scope="col">Adresse</th>
            <th scope="col">Code postal</th>
            <th scope="col">Ville</th>
            <th scope="col">Mail</th>
            <th scope="col">Mot de passe</th>
            <th scope="col">Modifier</th>
            <th scope="col">Supprimer</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($CompteListe as $uncompte) {
        ?>
          <tr>
            <th><?php echo $uncompte->num; ?></th>
            <td><?php echo utf8_encode($uncompte->prenom); ?></td>
            <td><?php echo utf8_encode($uncompte->nom); ?></td>
            <td><?php echo $uncompte->civilite; ?></td>
            <td><?php echo $uncompte->tel; ?></td>
            <td><?php echo $uncompte->dateNais; ?></td>
            <td><?php echo $uncompte->ad; ?></td>
            <td><?php echo $uncompte->cp; ?></td>
            <td><?php echo $uncompte->ville; ?></td>
            <td><?php echo utf8_encode($uncompte->mail); ?></td>
            <td><?php echo "**********"; ?></td>
            <td><a href='index.php?page=modifierCompte&num=<?php echo $uncompte->num; ?>'><i class="far fa-edit"></i></a></td>
            <td><a href='controleur/actionCompte.php?num=<?php echo $uncompte->num; ?>&delete=3'><i class="far fa-trash-alt"></i></a></td>
          </tr>
      <?php
        }
    }
    ?>
    </tbody>
</table>