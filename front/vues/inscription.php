<form name="frmInscription" method="POST" action="controleur/actionClient.php">
    
    <div class="form-group">
        <label for="civilite">Civilité</label>
        <select name="civilite" id="civilite" class="form-control">
            <option value="m" id="m" selected>M.</option>
            <option value="f" id="f">Mme.</option>
        </select>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="txtNom">Nom</label>
            <input type="text" class="form-control" name="txtNom" id="txtNom">
        </div>
        <div class="form-group col-md-6">
            <label for="txtPrenom">Prénom</label>
            <input type="text" class="form-control" name="txtPrenom" id="txtPrenom">
        </div>
    </div>
    <div class="form-group">
        <label for="adresse">Adresse (N°, voie, lieu-dit, ...)</label>
        <input type="text" class="form-control" name ="adresse" id="adresse" placeholder="1234 Avenue du Pic Saint Loup">
    </div>
    <div class="form-row">
        <div class="form-group col-md-8">
            <label for="ville">Ville</label>
            <input type="text" class="form-control" name="ville" id="ville">
        </div>
        <div class="form-group col-md-2">
            <label for="codePostal">Code Postal</label>
            <input type="text" class="form-control" name="codePostal" id="codePostal" maxLength="5">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="date">Date de naissance</label>
            <input type="date" class="form-control" name="date" id="date">
        </div>
        <div class="form-group col-md-6">
            <label for="txtTel">Numéro de téléphone</label>
            <input type="text" class="form-control" name="txtTel" id="txtTel" maxLength="10">
        </div>
    </div>
    <div class="form-row">
        <div class="form-group col-md-6">
            <label for="txtMel">Adresse mail</label>
            <input type="email" class="form-control" name="txtMel" id="txtMel">
        </div>
        <div class="form-group col-md-6">
            <label for="mdp">Mot de passe</label>
            <input type="password" class="form-control" name="mdp" id="mdp">
        </div>
    </div>
    <input type="hidden" name="action" value="1"/>
    <button type="submit" class="btn btn-danger">S'inscrire</button>
</form>


<!--
<h2>Inscription</h2>

<form name="frmInscription" method="POST" action="controleur/actionClient.php">
    <fieldset>
        <legend>Créer mon compte</legend>
        <fieldset>
            <legend>Informations Personnelles</legend>
            <table>
                <!-- civilite -->
                <!--<tr>
                    <td class="alignDroit"><label for="civilite">Civilité : </label></td>
                    <td class="alignGauche">
                        <select name="civilite" id="civilite">
                            <option value="m" id="m">M.</option>
                            <option value="f" id="f">Mme.</option>
                        </select>
                    </td>

                <!-- nom -->
                    <!--<td class="alignDroit"><label for="txtNom">Nom : </label></td>
                    <td class="alignGauche"><input type="text" name="txtNom" id="txtNom" required /></td>

                <!-- prenom -->
                    <!--<td class="alignDroit"><label for="txtPrenom">Prénom : </label></td>
                    <td class="alignGauche"><input type="text" name="txtPrenom" id="txtPrenom" /></td>
                </tr>

                <!-- adresse -->
                <!--<tr>
                    <td class="alignDroit"><label for="adresse">Adresse (N°, voie, lieu-dit, ...) :</label></td>
                    <td class="alignGauche"><input type="text" id="adresse" name="adresse" /></td>

                <!-- code postal -->
                    <!--<td class="alignDroit"><label for="codePostal">Code Postal :</label></td>
                    <td class="alignGauche"><input type="text" id="codePostal" name="codePostal" maxLength="5" /></td>

                <!-- ville -->
                    <!--<td class="alignDroit"><label for="ville">Ville :</label></td>
                    <td class="alignGauche"><input type="text" id="ville" name="ville" /><td>
                </tr>
                
                <!-- date de naissance -->
                <!--<tr>
                    <td class="alignDroit"><label for="date">Date de naissance :</label>
                    <td class="alignGauche"><input type="date" id="date" name="date" /></td>

                <!-- numero de telephone -->
                    <!--<td class="alignDroit"><label for="txtTel">Numéro de téléphone : </label></td>
                    <td class="alignGauche"><input type="text" name="txtTel" id="txtTel" onblur="VerifTel()" /><span id="etoileTel" class="messInfo"></span></td>
                </tr>
            </table>
            <p class="messInfo">
                <span id="msgTel"></span>
            </p>
        </fieldset>

        <fieldset>
            <legend>Informations Du Compte</legend>
            <table>
                <!-- adresse mail -->
                <!--<tr>
                    <td class="alignDroit"><label for="txtMel">Adresse électronique : </label></td>
                    <td class="alignGauche"><input type="text" name="txtMel" id="txtMel" required onblur="VerifMel()" /><span id="etoileMel" class="messInfo"></span></td>
                
                <!-- mdp -->
                    <!--<td class="alignDroit"><label for="mdp">Mot de passe :</label></td>
                    <td class="alignGauche"><input type="password" id="mdp" name="mdp" /></td>
                </tr>
            </table>
            <p class="messInfo">
                <span id="msgMel"></span>
            </p>
        </fieldset>
        <input type="hidden" name="action" value="1"/>
    </fieldset>
    <br />
    <input type="submit" name="btEnvoi" value="Enregistrer votre saisie" onclick="validerSaisie()" />
    <input type="reset" name="btRAZ" value="Réinitialiser votre saisie" onclick="effaceSpan()" />
    <span id="msgValid"></span>
</form>
<!--<script type="text/javascript" src="verifForm.js"></script>!-->