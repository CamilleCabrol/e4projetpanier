<link rel="stylesheet" href="style.css">

<?php
if (count($ProduitsListe) == 0) {
?>
    <h2>Aucun produit dans la bdd</h2>
<?php
} 
else {
?>
    <table class="table table-hover" id="Tableau">
        <thead>
            <tr>
                <th scope="col">Identifiant</th>
                <th scope="col">Nom</th>
                <th scope="col">Photo</th>
                <th scope="col">Prix</th>
                <th scope="col">Numéro Catégorie</th>
                <th scope="col">Modifier</th>
                <th scope="col">Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($ProduitsListe as $unProduit) {
            ?>
            <tr>
                <th><?php echo $unProduit->num; ?></th>
                <td><?php echo $unProduit->nom; ?></td>
                <td><div class="img-prod"><?php echo "<img src='images/" . $unProduit->image . "'>"; ?></div></td>
                <td><?php if ($unProduit->prix == 1 || $unProduit->prix == 2) {
                    echo $unProduit->prix . ".00 €";
                } else {
                    echo $unProduit->prix. " €";
                } ?></td>
                <td><?php echo $unProduit->categ; ?></td>
                <td><a href='index.php?page=modifierProduit&num=<?php echo $unProduit->num; ?>'><i class="far fa-edit"></i></a></td>
                <td><a href='controleur/actionProduit.php?num=<?php echo $unProduit->num; ?>&action=3'><i class="far fa-trash-alt"></i></a></td>
            </tr>
            <?php
                }
    }
            ?>
        </tbody>
    </table>