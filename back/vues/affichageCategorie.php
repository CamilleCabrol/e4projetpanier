<?php
if (count($CategoriesListe) == 0) {
?>
    <h2> Il n'y a aucune catégorie </h2>
<?php
} 
else {
?>
    <table class="table table-hover" id="Tableau">
        <thead>
            <tr>
                <th scope="col">Identifiant</th>
                <th scope="col">Catégorie</th>
                <th scope="col">Modifier</th>
                <th scope="col">Supprimer</th>
            </tr>
        </thead>
        <tbody>
            <?php
                foreach ($CategoriesListe as $uneCateg) {
            ?>
            <tr>
                <th><?php echo $uneCateg->num; ?></th>
                <td><?php echo $uneCateg->nom; ?></td>
                <td><a href='index.php?page=modifierCategorie&num=<?php echo $uneCateg->num; ?>'><i class="far fa-edit"></i></a></td>
                <td><a href='controleur/actionCategorie.php?num=<?php echo $uneCateg->num; ?>&action=3'><i class="far fa-trash-alt"></i></a></td>
            </tr>
            <?php
                }
    }
            ?>
        </tbody>
    </table>
    
    

    
    
    
    
    
 