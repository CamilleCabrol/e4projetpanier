<h1>Page d'accueil</h1>

<p>Qui a besoin d’une excuse pour acheter des bonbons ? Personne! Une boîte de bonbons met tout le monde d’accord,
    que l’on aille à l’école ou au bureau, à une soirée entre amis ou à un mariage. Dans notre boutique de bonbons,
    vous retrouverez toutes les confiseries qui ont fait notre succès. Une véritable explosion de couleurs, de
    textures et de goûts ! Nous sommes même certains que vous avez déjà votre préféré… et si ce n’est pas le cas, 
    voilà une raison de plus d’acheter des bonbons ! Après tout, impossible de rester sans savoir quel est votre 
    bonbon préféré!</p>