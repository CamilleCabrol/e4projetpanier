-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : mer. 02 déc. 2020 à 14:41
-- Version du serveur :  5.7.31
-- Version de PHP : 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `e4_projetpanier`
--

-- --------------------------------------------------------

--
-- Structure de la table `articles`
--

DROP TABLE IF EXISTS `articles`;
CREATE TABLE IF NOT EXISTS `articles` (
  `numArt` int(2) NOT NULL AUTO_INCREMENT,
  `nomArt` varchar(100) NOT NULL,
  `image` varchar(100) NOT NULL,
  `prix` double NOT NULL,
  `numCateg` int(2) NOT NULL,
  PRIMARY KEY (`numArt`),
  KEY `fk_numCateg` (`numCateg`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `articles`
--

INSERT INTO `articles` (`numArt`, `nomArt`, `image`, `prix`, `numCateg`) VALUES
(2, 'Happy Life Pik', 'bonbonquipik.jpg', 2.25, 1),
(3, 'Cerises', 'cerises.jpg', 2, 2),
(4, 'FlowerPower', 'flowerpower.jpg', 1.5, 2),
(5, 'Frites', 'frites.jpg', 2, 1),
(6, 'Krema', 'krema.jpg', 2.5, 2),
(8, 'Schtroumpfs', 'schtroumpfs.jpg', 2.25, 2),
(9, 'Soucoupes', 'soucoupes.jpg', 2, 2),
(10, 'Vers de terre', 'versdeterre.jpg', 2.5, 1);

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `numCateg` int(2) NOT NULL AUTO_INCREMENT,
  `libCateg` varchar(100) NOT NULL,
  PRIMARY KEY (`numCateg`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`numCateg`, `libCateg`) VALUES
(1, 'BonbonQuiPik'),
(2, 'BonbonToutDoux');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `numCli` int(10) NOT NULL AUTO_INCREMENT,
  `civilite` varchar(3) NOT NULL,
  `nomCli` varchar(100) NOT NULL,
  `prenomCli` varchar(100) NOT NULL,
  `telephoneCli` int(10) NOT NULL,
  `dateNaisCli` date NOT NULL,
  `adCli` varchar(100) NOT NULL,
  `villeCli` varchar(50) NOT NULL,
  `cpCli` int(5) NOT NULL,
  `mailCli` varchar(100) NOT NULL,
  `mdpCli` varchar(100) NOT NULL,
  `isAdmin` bit(1) NOT NULL DEFAULT b'0',
  PRIMARY KEY (`numCli`),
  UNIQUE KEY `mailCli` (`mailCli`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`numCli`, `civilite`, `nomCli`, `prenomCli`, `telephoneCli`, `dateNaisCli`, `adCli`, `villeCli`, `cpCli`, `mailCli`, `mdpCli`, `isAdmin`) VALUES
(1, 'm', 'BERAUD', 'Quentin', 615243589, '2020-09-16', '12 rue kjdkazbd', 'Montpellier', 34000, 'qberaud@gmail.fr', '$2y$10$u7rFAL7jil9CJtMeZSpXkevb09Y6AiiC7TAPXiri.YFP4eta.Hdlu', b'0'),
(3, 'm', 'PREVOT', 'Jonas', 668511042, '1997-12-19', '12 Rue Hector Berlioz', 'Saint Martin D\'Heres', 38400, 'jonasprevot@hotmail.com', '$2y$10$I5OP03tgxsdo1DdtRFLGOuSCqPVa328JnEwVAteKilcrfng9gVqIO', b'0'),
(4, 'f', 'CABROL', 'Camille', 695885927, '1999-12-21', '515 avenue Jean Mermoz', 'Montpellier', 34000, 'camille.cabrol@free.fr', '$2y$10$zRzQD1npvQRZuLqQ1x9E8eNQeIjRsXWdafXTe06iRZyPgyCX53.p6', b'1'),
(5, 'm', 'CABROL', 'Maxime', 784569821, '2002-05-29', '12 rue Jean Mermoz', 'Agde', 34300, 'maxime.cabrol@free.fr', '$2y$10$hIWW3C5cUnG9POZR7YtZg.Sya.Kdr8QT3dxQjrhaOjEi1m2utNcci', b'0');

-- --------------------------------------------------------

--
-- Structure de la table `commande`
--

DROP TABLE IF EXISTS `commande`;
CREATE TABLE IF NOT EXISTS `commande` (
  `numCmd` int(11) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `numCli` int(11) NOT NULL,
  PRIMARY KEY (`numCmd`),
  KEY `fk_commande_client` (`numCli`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `ligne_commande`
--

DROP TABLE IF EXISTS `ligne_commande`;
CREATE TABLE IF NOT EXISTS `ligne_commande` (
  `numCmd` int(10) NOT NULL,
  `numArt` int(2) NOT NULL,
  `quantite` int(3) NOT NULL,
  PRIMARY KEY (`numCmd`,`numArt`),
  KEY `fk_ligneCmd_article` (`numArt`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `articles`
--
ALTER TABLE `articles`
  ADD CONSTRAINT `fk_numCateg` FOREIGN KEY (`numCateg`) REFERENCES `categorie` (`numCateg`);

--
-- Contraintes pour la table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `fk_commande_client` FOREIGN KEY (`numCli`) REFERENCES `client` (`numCli`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `ligne_commande`
--
ALTER TABLE `ligne_commande`
  ADD CONSTRAINT `fk_ligneCmd_article` FOREIGN KEY (`numArt`) REFERENCES `articles` (`numArt`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_ligneCmd_commande` FOREIGN KEY (`numCmd`) REFERENCES `commande` (`numCmd`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
