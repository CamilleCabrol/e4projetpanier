<?php

require_once("Compte.class.php");

class GestionCompte {
    
    public static function getLesComptes()
    {
        require("connectBDD.php");
        try {
            $lesComptes = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->query('SELECT * FROM CLIENT');
            while ($donnees = $req->fetch()) {
                $lesComptes[] = new Compte($donnees['numCli'], $donnees["civilite"], utf8_encode($donnees["nomCli"]), utf8_encode($donnees["prenomCli"]), $donnees["telephoneCli"], $donnees["dateNaisCli"], $donnees["adCli"], $donnees['villeCli'], $donnees["cpCli"], $donnees["mailCli"], $donnees["mdpCli"]);
            }
            $req->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesComptes;
    }
    
    public static function GetUnCompte($id)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('SELECT * FROM CLIENT WHERE numCli=?');
            $req->execute(array($id));
            $donnees = $req->fetch();
            $unCompte = new Compte($donnees['numCli'], $donnees["civilite"], utf8_encode($donnees["nomCli"]), utf8_encode($donnees["prenomCli"]), $donnees["telephoneCli"], $donnees["dateNaisCli"], $donnees["adCli"], $donnees['villeCli'], $donnees["cpCli"], $donnees["mailCli"], $donnees["mdpCli"]);
            $req->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $unCompte;
    }
    
    public static function AjouterUnCompte($prenom, $nom, $civilite, $tel, $date, $adresse, $cp, $ville, $mail, $mdp)
    {
        require("connectBDD.php");
        try {
            $pass_hache = password_hash($mdp, PASSWORD_DEFAULT);
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('INSERT INTO CLIENT (civilite, nomCli, prenomCli, telephoneCli, dateNaisCli, adCli, villeCli, cpCli, mailCli, mdpCli) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)');
            $req->execute(array($civilite, $nom, $prenom, $tel, $date, $adresse, $ville, $cp, $mail, $pass_hache));
            $req->closeCursor();  // ferme le curseur
            //echo "Le compte a été ajouté !";
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    
    public static function ModifierCompte($id, $prenom, $nom, $civilite, $tel, $date, $adresse, $cp, $ville, $mail, $mdp)
    {
        require("connectBDD.php");
        try {
            if (!$mdp == '') {
                $pass_hache = password_hash($mdp, PASSWORD_DEFAULT);
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
                $req = $bdd->prepare('UPDATE CLIENT SET civilite=?, nomCli=?, prenomCli=?, telephoneCli=?, dateNaisCli=?, adCli=?, villeCli=?, cpCli=?, mailCli=?, mdpCli=? WHERE numCli=?');
                $req->execute(array($civilite, $nom, $prenom, $tel, $date, $adresse, $ville, $cp, $mail, $pass_hache, $id));
                //echo "Le compte a été modifié !";
            } else {
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
                $req = $bdd->prepare('UPDATE CLIENT SET civilite=?, nomCli=?, prenomCli=?, telephoneCli=?, dateNaisCli=?, adCli=?, villeCli=?, cpCli=?, mailCli=? where numCli=?');
                $req->execute(array($civilite, $nom, $prenom, $tel, $date, $adresse, $ville, $cp, $mail, $id));
                //echo "Le compte a été modifié !";
            }
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function SupprimerUnCompte($id)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('DELETE FROM CLIENT WHERE numCli=?');
            $req->execute(array($id));
            //echo "Le compte a été supprimé !";
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
}
