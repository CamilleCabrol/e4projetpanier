<?php

include_once("../modeles/ClassePasserelle.php");

if (isset($_GET["action"])) {
    $action = $_GET["action"];
    /* On vérifie l'existence du panier, sinon, on le crée */
    if(!isset($_SESSION['panier'])) {
        /* Initialisation du panier */
        $_SESSION['panier'] = array();
    }
    switch ($action) {
        case 1:
            $numProduit = $_GET["numProduit"];
            GestionPanier::ajoutProduit($numProduit);
            header("Location: ../index.php?page=affichagePanier");
            break;

        case 2:
            GestionPanier::supprimerProduit($_GET['num']);
            header('Location: ../index.php?page=affichagePanier');
            break;

        case 3:
            $montant = GestionPanier::montantPanier();
            header('Location: ../index.php?page=espaceClient');
            break;

        case 4:
            $retour = GestionPanier::viderPanier();
            if ($retour == "inexistant" || $retour == false) {
                //le panier a déjà été détruit ou n'a jamais été créée ou le panier n'a pas pu être détruit
                header('Location: ../index.php?page=connection&erreur='.$retour);
            } else {
                header('Location: ../index.php?page=espaceClient');
            }
            break;
            
        case 5 :
            GestionPanier::modifierQte($_GET["numProduit"], $_GET['qte']);
            header('Location: ../index.php?page=affichagePanier');
            break;
    }
}
