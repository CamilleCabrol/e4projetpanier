<?php

class Categorie {

    private $num;
    private $nom;

    function __construct($unNum, $unNom) {
        $this->num = $unNum;
        $this->nom = $unNom;
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : $this->num = $valeur;
                    break;
                case 'nom' : $this->nom = $valeur;
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : return $this->num;
                case 'nom' : return $this->nom;
            }
        }
    }

    function __toString() {
        return "Num : " . $this->num . ". " . $this->nom;
    }
}