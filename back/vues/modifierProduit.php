<form name='monform' method='GET' action='controleur/actionProduit.php'>
    
    <div class="form-row">
        <div class="form-group col-md-3">
            <label for="nom">Nom</label>
            <input type="text" class="form-control" name='nom' id="nom" value='<?php echo $DetailProduit->nom;?>'>
        </div>
        <div class="form-group col-md-3">
            <label for="image">Photo (nom de l'image)</label>
            <input type="text" class="form-control" name='image' id="image" value='<?php echo $DetailProduit->image; ?>'>
        </div>
        <div class="form-group col-md-3">
            <label for="prix">Prix (en €)</label>
            <input type="text" class="form-control" name='prix' id="prix" value='<?php echo $DetailProduit->prix; ?>'>
        </div>
        <div class="form-group col-md-3">
            <label for="categ">Numéro Catégorie</label>
            <select name='categ' id="categ" class="form-control">
                <option value="1" id="1" <?php if($DetailProduit->categ == '1') { echo 'selected'; } ?>>1</option>
                <option value="2" id="2" <?php if($DetailProduit->categ == '2') { echo 'selected'; } ?>>2</option>
            </select>
        </div>
    </div>
    <input type='hidden' name='num' id='num' value='<?php echo $DetailProduit->num;?>' />
    <input type='hidden' name='action' value='2' />
    <button class="btn btn-danger" type="submit" name="btEnvoi">Valider</button>
    <button class="btn btn-danger" onclick="window.location.href = 'index.php?page=affichageProduit';">Annuler</button>
</form>