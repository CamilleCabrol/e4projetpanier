<?php

include("../modeles/gestionProduit.class.php");

$action = $_GET["action"];
switch ($action) {
    case 1:
        GestionProduit::ajouterProd($_GET['nom'], $_GET['image'], $_GET['prix'], $_GET['categ']);
        break;

    case 2:
        GestionProduit::modifierProd($_GET['num'], $_GET['nom'], $_GET['image'], $_GET['prix'], $_GET['categ']);
        break;

    case 3:
        GestionProduit::supprimerProduit($_GET['num']);
        break;
}

header("Location: ../index.php?page=affichageProduit"); //affiche les occurrences après l'ajout
?>