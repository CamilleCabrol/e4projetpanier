<?php

class Categorie {

    private $num;
    private $nom;

    function __construct($unNum, $unNom) {
        $this->num = $unNum;
        $this->nom = $unNom;
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : $this->num = $valeur;
                    break;
                case 'nom' : $this->nom = $valeur;
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : return $this->num;
                case 'nom' : return $this->nom;
            }
        }
    }

    function __toString() {
        return "Num : " . $this->num . ". " . $this->nom;
    }
}

class Client {

    private $num;
    private $civilite;
    private $nom;
    private $prenom;
    private $tel;
    private $dateNais;
    private $ad;
    private $ville;
    private $cp;
    private $mail;
    private $mdp;

    function __construct($unNum, $uneCivilite, $unNom, $unPrenom, $unTel, $uneDate, $uneAd, $uneVille, $unCp, $unMail, $unMdp) {
        $this->num = $unNum;
        $this->civilite = $uneCivilite;
        $this->nom = $unNom;
        $this->prenom = $unPrenom;
        $this->tel = $unTel;
        $this->dateNais = $uneDate;
        $this->ad = $uneAd;
        $this->ville = $uneVille;
        $this->cp = $unCp;
        $this->mail = $unMail;
        $this->mdp = password_hash($unMdp, PASSWORD_DEFAULT);
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : $this->num = $valeur;
                    break;
                case 'civilite' : $this->civilite = $valeur;
                    break;
                case 'nom' : $this->nom = $valeur;
                    break;
                case 'prenom' : $this->prenom = $valeur;
                    break;
                case 'tel' : $this->tel = $valeur;
                    break;
                case 'dateNais' : $this->dateNais = $valeur;
                    break;
                case 'ad' : $this->ad = $valeur;
                    break;
                case 'ville' : $this->ville = $valeur;
                    break;
                case 'cp' : $this->cp = $valeur;
                    break;
                case 'mail' : $this->mail = $valeur;
                    break;
                case 'mdp' : $this->mdp = password_hash($valeur, PASSWORD_DEFAULT);
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : return $this->num;
                case 'civilite' : return $this->civilite;
                case 'nom' : return $this->nom;
                case 'prenom' : return $this->prenom;
                case 'tel' : return $this->tel;
                case 'dateNais' : return $this->dateNais;
                case 'ad' : return $this->ad;
                case 'ville' : return $this->ville;
                case 'cp' : return $this->cp;
                case 'mail' : return $this->mail;
                case 'mdp' : return $this->mdp;
            }
        }
    }

    function __toString() {
        return $this->nom . " " . $this->prenom;
    }

}

class Produit {

    private $num;
    private $nom;
    private $image;
    private $prix;
    private $categ;

    function __construct($unNum, $unNom, $uneImage, $unPrix, $uneCatg) {
        $this->num = $unNum;
        $this->nom = $unNom;
        $this->image = $uneImage;
        $this->prix = $unPrix;
        $this->categ = $uneCatg;
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : $this->num = $valeur;
                    break;
                case 'nom' : $this->nom = $valeur;
                    break;
                case 'image' : $this->image = $valeur;
                    break;
                case 'prix' : $this->prix = $valeur;
                    break;
                case 'categ' : $this->categ = $valeur;
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : return $this->num;
                case 'nom' : return $this->nom;
                case 'image' : return $this->image;
                case 'prix' : return $this->prix;
                case 'categ' : return $this->categ;
            }
        }
    }

    function __toString() {
        return $this->nom;
    }

}

class Commande {

    private $numCli;
    private $numArt;
    private $qte;

    function __construct($unNumCli, $unNumArt, $uneQte) {
        $this->numCli = $unNumCli;
        $this->numArt = $unNumArt;
        $this->qte = $uneQte;
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'numCli' : $this->numCli = $valeur;
                    break;
                case 'numArt' : $this->numArt = $valeur;
                    break;
                case 'qte' : $this->qte = $valeur;
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'numCli' : return $this->numCli;
                case 'numArt' : return $this->numArt;
                case 'qte' : return $this->qte;
            }
        }
    }

    function __toString() {
        return $this->qte.'articles.';
    }
}

?>