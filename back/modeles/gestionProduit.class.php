<?php

include_once("Produit.class.php");

class GestionProduit {

// Fonction de suppression d'un produit identifié par son num

    public static function supprimerProduit($num) {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('DELETE FROM ARTICLES WHERE numArt=?;');
            $reponse->execute(array($num));
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

// Modification d'un produit identifié par son num

    public static function modifierProd($num, $nom, $image, $prix, $numCateg) {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('UPDATE ARTICLES SET nomArt=? AND image=? AND prix=? AND numCateg=? WHERE numArt= ?;');
            $reponse->execute(array($nom, $image, $prix, $numCateg, $num));
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

//Ajout d'un produit

    public static function ajouterProd($nom, $image, $prix, $categ) {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('INSERT INTO ARTICLES(nomArt, image, prix, numCateg) VALUES (?,?,?)');
            $reponse->execute(array($nom, $image, $prix, $categ));
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

// Récupération de la liste des produits

    public static function getUnProd($num) {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numArt, nomArt, image, prix, numCateg FROM ARTICLES WHERE numArt=?;');
            $reponse->execute(array($num));
            $donnees = $reponse->fetch();
            $unProd = new Produit($donnees["numArt"], $donnees["nomArt"], $donnees["image"], $donnees["prix"], $donnees["numCateg"]);
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $unProd;
    }

    public static function getLesProduits() {
        require("connectBDD.php");
        try {
            $lesProduits = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->query('SELECT numArt, nomArt, image, prix, numCateg FROM ARTICLES');
            while($donnees = $reponse->fetch()){
                $lesProduits[]=new Produit($donnees["numArt"], $donnees["nomArt"], $donnees["image"], $donnees["prix"], $donnees["numCateg"]);
            }
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesProduits;
    }

}

?>