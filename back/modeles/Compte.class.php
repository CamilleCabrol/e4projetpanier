<?php

class Compte {

    private $num;
    private $civilite;
    private $nom;
    private $prenom;
    private $tel;
    private $dateNais;
    private $ad;
    private $ville;
    private $cp;
    private $mail;
    private $mdp;
    private $isAdmin;

    function __construct($unNum, $unPrenom, $unMdp) {
        $this->num = $unNum;
        $this->prenom = $unPrenom;
        $this->mdp = $unMdp;
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : $this->num = $valeur;
                    break;
                case 'civilite' : $this->civilite = $valeur;
                    break;
                case 'nom' : $this->nom = $valeur;
                    break;
                case 'prenom' : $this->prenom = $valeur;
                    break;
                case 'tel' : $this->tel = $valeur;
                    break;
                case 'dateNais' : $this->dateNais = $valeur;
                    break;
                case 'ad' : $this->ad = $valeur;
                    break;
                case 'ville' : $this->ville = $valeur;
                    break;
                case 'cp' : $this->cp = $valeur;
                    break;
                case 'mail' : $this->mail = $valeur;
                    break;
                case 'mdp' : $this->mdp = password_hash($valeur, PASSWORD_DEFAULT);
                    break;
                case 'isAdmin' : $this->isAdmin = $valeur;
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : return $this->num;
                case 'civilite' : return $this->civilite;
                case 'nom' : return $this->nom;
                case 'prenom' : return $this->prenom;
                case 'tel' : return $this->tel;
                case 'dateNais' : return $this->dateNais;
                case 'ad' : return $this->ad;
                case 'ville' : return $this->ville;
                case 'cp' : return $this->cp;
                case 'mail' : return $this->mail;
                case 'mdp' : return $this->mdp;
                case 'isAdmin' : return $this->isAdmin;
            }
        }
    }

    function __toString() {
        return $this->nom . " " . $this->prenom;
    }
}