<!--<footer class="page-footer font-small blue">
    <div class="footer-copyright text-center py-3">
        <?php /*echo "©" . date('Y'); */?> - Tous droits reservés - Camille CABROL.
    </div>
</footer>-->

<footer class="page-footer font-small blue" style="background-color: #f8f9fa; margin-top: 50px;">
    <div class="footer-copyright text-center py-3">
        Développé dans le cadre de mon BTS SIO.
        <br>
        <?php echo "©" . date('Y'); ?> - Tous droits reservés - Camille CABROL.
    </div>
</footer>