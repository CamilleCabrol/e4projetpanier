<?php

include("../modeles/gestionCategorie.class.php");

$action = $_GET["action"];
switch ($action) {
    case 1:
        GestionCategorie::AjouterUneCategorie($_GET['nom']);
        break;

    case 2:
        GestionCategorie::ModifierUneCategorie($_GET['num'], $_GET['nom']);
        break;

    case 3:
        GestionCategorie::SupprimerUneCategorie($_GET['num']);
        break;
}

header("Location: ../index.php?page=affichageCategorie"); //affiche les occurrences après l'ajout
?>