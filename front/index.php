<?php
if(session_status()== PHP_SESSION_NONE)
{
    session_start();
}
if(isset($_GET['action'])) {
    $action=$_GET["action"];
    if($action == "deconnexion") {
        session_destroy();
        header("Location:index.php");
    }
}
?>

<!DOCTYPE html>
<html>

    <head>
        <title>BonbonTropBon</title>
        <meta charset="UTF-8" />
        <link rel="icon" href="images/logo.jpg" type="image/x-icon" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script>
        <script src="https://use.fontawesome.com/releases/v5.12.1/js/all.js" data-auto-replace-svg="nest"></script>
    </head>

    <body background="images/fondpage.jpg" >
        <?php
        include "vues/include/header.inc.php";
        ?>
        <div>
            <?php
            include "vues/include/nav.inc.php";
            ?>
            <div class="container" style='overflow: auto;'>
                <br/>
                <?php
                if (!isset($_GET['page'])) {
                    include "vues/include/index.inc.php";
                } else {
                    $page = $_GET["page"];
                    if (file_exists("controleur/$page.php")) {
                        include "controleur/$page.php";
                    } else {
                        include "vues/include/error.inc.php";
                    }
                }
                ?>
            </div>
        </div>
        <?php
        include "vues/include/footer.inc.php";
        ?>
    </body>
    
</html>