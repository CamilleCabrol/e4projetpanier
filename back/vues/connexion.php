<!--
<form id="frmSaisie" method="POST" action="controleur/actionAdmin.php">
    <fieldset>
        <legend>Entrez vos informations</legend>
        <table>
            <!-- mail -->
            <!--
            <tr>
                <td class="alignDroit"><label for="mail">Adresse mail : </label></td> <!--le for redirige au input qui porte le même nom-->
                <!--
                <td class="alignGauche"><input type="text" name="mail" id="mail" /></td>

                <!-- mdp -->
                <!--
                <td class="alignDroit"><label for="mdp">Mot de passe :</label></td>
                <td class="alignGauche"><input type="password" id="mdp" name="mdp" /></td>
            </tr>
        </table>
    </fieldset>
    <input type="hidden" name="action" value="2"/>
    <input type="submit" name="btEnvoi" value="Se connecter" />

    <?php
    /*
    if(isset($_GET['erreur'])){
        $err = $_GET['erreur'];
        if($err == 1){
            echo "<p>Email ou mot de passe incorrect</p>";
        }
    }
    */
    ?>
</form>-->
<h1>Bonjour ! Avant toute chose veuillez vous connecter</h1>
<br/>
<form id="frmSaisie" method="POST" action="controleur/actionAdmin.php">
    <div class="form-group">
        <label for="mail">Adresse mail</label>
        <input type="email" class="form-control" name="mail" id="mail" aria-describedby="emailHelp">
    </div>
    <div class="form-group">
        <label for="mdp">Mot de passe</label>
        <input type="password" class="form-control" name="mdp" id="mdp">
    </div>
    <input type="hidden" name="action" value="2"/>
    <button type="submit" class="btn btn-danger" name="btEnvoi">Se connecter</button>
    
    <?php
        if(isset($_GET['erreur'])){
            $err = $_GET['erreur'];
            if($err == 1){
                echo "<p>Email ou mot de passe incorrect</p>";
            }
        }
    ?>
</form>