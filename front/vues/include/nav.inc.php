<?php
include_once("controleur/listeCategorie.php");
?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" >BonbonTropBon</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item active">
                <a class="nav-link" href="index.php">Accueil<span class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="index.php?page=listeProduit" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Nos bonbons </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <?php
                    foreach ($lesCateg as $uneCateg) {
                    ?>
                    <a class="dropdown-item" href="index.php?page=listeProdParCateg&numCateg=<?php echo $uneCateg->num; ?>"><?php echo utf8_encode($uneCateg->nom); ?></a>
                    <?php } ?>
                    <a class="dropdown-item" href="index.php?page=listeProduit">Tous les bonbons</a>
                </div>
            </li>
            <?php if (isset($_SESSION['numCli'])) { ?>
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=espaceClient">Mon profil</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" href="index.php?action=deconnexion">Me déconnecter</a>
            </li>
            <?php } else {
            ?>
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=inscription">Inscription</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="index.php?page=connection">Me connecter</a>
            </li>
            <?php }
            ?>
        </ul>
    </div>
    
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link p-2" href="index.php?page=affichagePanier" >Mon panier <i class="fas fa-shopping-cart"></i></a>
        </li>
    </ul>
        

</nav>

<!-- <nav>
    <ul class="nav">
        <li class="active">
            <a href="index.php">Accueil</a>
        </li>
        <li class="dropdown">
            <a href="index.php?page=listeProduit" class="dropbtn">Les produits</a>
            <div class="dropdown-content">
<?php /*
  foreach ($lesCateg as $uneCateg) {
  ?>
  <a href="index.php?page=listeProdParCateg&numCateg=<?php echo $uneCateg->num;?>"><?php echo utf8_encode($uneCateg->nom); ?></a>
  <?php /*} */ ?>
            </div>
        </li>
        <li>
            <a href="index.php?page=frmClient">Inscription</a>
        </li>
    </ul>
</nav>


<!--<div class="navbar">
    <a>BonbonTropBon</a>
    <a href="index.php">Accueil</a>
    <div class="dropdown">
        <button class="dropbtn" onclick="window.location.href = 'index.php?page=listeProduit';">Nos bonbons 
            <i class="fa fa-caret-down"></i>
        </button>
        <div class="dropdown-content">
<?php
/*
          foreach ($lesCateg as $uneCateg) {
          ?>
          <a href="index.php?page=listeProdParCateg&numCateg=<?php echo $uneCateg->num; ?>"><?php echo utf8_encode($uneCateg->nom); ?></a>
          <?php /* }*/?>
                  </div>
                  <?php /*if (isset($_SESSION['numCli'])) { */?>
                  <a href="index.php?page=espaceClient">Mon profil</a>
                  <a href="index.php?action=deconnexion">Me déconnecter</a>
                  <?php /*} else {*/
                  ?>
                  <a href="index.php?page=inscription">Inscription</a>
                  <a href="index.php?page=connection">Me connecter</a>
                  <?php/* }*/
                  ?>
                  </div>
                  </div>-->



                 