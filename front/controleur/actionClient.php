<?php

include_once("../modeles/ClassePasserelle.php");

if (isset($_POST["action"])) {
    $action = $_POST["action"];
    switch ($action) {
        case 1:
            GestionClient::inscriptionCli($_POST['civilite'], $_POST['txtNom'], $_POST['txtPrenom'], $_POST['txtTel'], $_POST['date'], $_POST['adresse'], $_POST['ville'], $_POST['codePostal'], $_POST['txtMel'], $_POST['mdp']);
            header("Location: ../index.php");
            break;

        case 2:
            $retour = GestionClient::connectionCli($_POST['txtMel'], $_POST['mdp']);
            if ($retour != 0) {
                header('Location: ../index.php?page=connection&erreur='.$retour);
            } else {
                header('Location: ../index.php?page=espaceClient');
            }
            break;

        case 3:
            GestionClient::modifierInfoCli($_POST['numCli'], $_POST['civilite'], $_POST['txtNom'], $_POST['txtPrenom'], $_POST['txtTel'], $_POST['date'], $_POST['adresse'], $_POST['ville'], $_POST['codePostal']);
            header('Location: ../index.php?page=espaceClient');
            break;

        case 4:
            GestionClient::modifierCoCli($_POST['numCli'], $_POST['txtMel'], $_POST['mdp']);
            header('Location: ../index.php?page=espaceClient');
            break;
    }
}
