<?php

class Produit {

    private $num;
    private $nom;
    private $image;
    private $prix;
    private $categ;

    function __construct($unNum, $unNom, $uneImage, $unPrix, $uneCatg) {
        $this->num = $unNum;
        $this->nom = $unNom;
        $this->image = $uneImage;
        $this->prix = $unPrix;
        $this->categ = $uneCatg;
    }

    public function __set($attribut, $valeur) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : $this->num = $valeur;
                    break;
                case 'nom' : $this->nom = $valeur;
                    break;
                case 'image' : $this->image = $valeur;
                    break;
                case 'prix' : $this->prix = $valeur;
                    break;
                case 'categ' : $this->categ = $valeur;
                    break;
            }
        }
    }

    public function __get($attribut) {
        if (property_exists($this, $attribut)) {
            switch ($attribut) {
                case 'num' : return $this->num;
                case 'nom' : return $this->nom;
                case 'image' : return $this->image;
                case 'prix' : return $this->prix;
                case 'categ' : return $this->categ;
            }
        }
    }

    function __toString() {
        return $this->nom;
    }

}