<?php
if(session_status()== PHP_SESSION_NONE)
{
    session_start();
}

include_once("ClasseMetier.php");

class AffichageCateg
{

    public static function getUneCateg($num)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numCateg, libCateg FROM CATEGORIE WHERE numCateg=?;');
            $reponse->execute(array($num));
            $donnees = $reponse->fetch();
            $uneCateg = new Categorie($donnees["numCateg"], $donnees["libCateg"]);
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $uneCateg;
    }

    public static function getLesCateg()
    {
        require("connectBDD.php");
        try {
            $lesCateg = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->query('SELECT * FROM CATEGORIE');
            while ($donnees = $reponse->fetch()) {
                $lesCateg[] = new Categorie($donnees["numCateg"], $donnees["libCateg"]);
            }
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesCateg;
    }
}

class AffichageProduit
{

    public static function getLesProduits()
    {
        require("connectBDD.php");
        try {
            $lesProduits = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->query('SELECT * FROM ARTICLES');
            while ($donnees = $reponse->fetch()) {
                $lesProduits[] = new Produit($donnees["numArt"], $donnees["nomArt"], $donnees["image"], $donnees["prix"], $donnees["numCateg"]);
            }
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesProduits;
    }

    public static function getProdParCateg($numC)
    {
        require("connectBDD.php");
        try {
            $lesProduits = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numArt, nomArt, image, prix FROM ARTICLES WHERE numCateg=?');
            $reponse->execute(array($numC));
            while ($donnees = $reponse->fetch()) {
                $lesProduits[] = new Produit($donnees["numArt"], $donnees["nomArt"], $donnees["image"], $donnees["prix"], $numC);
            }
            $reponse->closeCursor();
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesProduits;
    }
    
    public static function getProdParNum($num)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numArt, nomArt, image, prix, numCateg FROM ARTICLES WHERE numArt=?');
            $reponse->execute(array($num));
            $donnees = $reponse->fetch();
            $unProduit = new Produit($donnees["numArt"], $donnees["nomArt"], $donnees["image"], $donnees["prix"], $donnees["numCateg"]);
            $reponse->closeCursor();
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $unProduit;
    }
}

class GestionClient
{

    public static function inscriptionCli($uneCivilite, $unNom, $unPrenom, $unTel, $uneDate, $uneAd, $uneVille, $unCp, $unMail, $unMdp)
    {
        require("connectBDD.php");
        try {
            $mdp_hash = password_hash($unMdp, PASSWORD_DEFAULT);
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('INSERT INTO CLIENT (civilite, nomCli, prenomCli, telephoneCli, dateNaisCli, adCli, villeCli, cpCli, mailCli, mdpCli) VALUES (?,?,?,?,?,?,?,?,?,?)');
            $reponse->execute(array($uneCivilite, $unNom, $unPrenom, $unTel, $uneDate, $uneAd, $uneVille, $unCp, $unMail, $mdp_hash));
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function connectionCli($unMail, $unMdp)
    {
        require("connectBDD.php");
        $retour = 1;
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numCli, prenomCli, mdpCli FROM CLIENT WHERE mailCli = ?');
            $reponse->execute(array($unMail));
            $donnees = $reponse->fetch();
            if ($donnees) {
                $verifMdp = password_verify($unMdp, $donnees['mdpCli']);
                if ($verifMdp) {
                    $retour = 0;
                    session_start();
                    $_SESSION['numCli'] = $donnees['numCli'];
                    $_SESSION['prenomCli'] = $donnees['prenomCli'];
                } else {
                    $retour = 2;
                }
            }
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $retour;
    }

    public static function modifierInfoCli($unNum, $uneCivilite, $unNom, $unPrenom, $unTel, $uneDate, $uneAd, $uneVille, $unCp)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('UPDATE CLIENT SET
            civilite=?, nomCli=?, prenomCli=?, telephoneCli=?, dateNaisCli=?,
            adCli=?, villeCli=?, cpCli=? WHERE numCli = ?');
            $reponse->execute(array(
                $uneCivilite, $unNom, $unPrenom, $unTel, $uneDate,
                $uneAd, $uneVille, $unCp, $unNum
            ));
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function modifierCoCli($unNum, $unMail, $unMdp)
    {
        require("connectBDD.php");
        try {
            if (!$unMdp == "") {
                $mdp_hash = password_hash($unMdp, PASSWORD_DEFAULT);
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
                $reponse = $bdd->prepare('UPDATE CLIENT SET
                mailCli=?, mdpCli=? WHERE numCli = ?');
                $reponse->execute(array($unMail, $mdp_hash, $unNum));
            } else {
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
                $reponse = $bdd->prepare('UPDATE CLIENT SET mailCli=? WHERE numCli = ?');
                $reponse->execute(array($unMail, $unNum));
            }
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function getUnClient($num)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numCli, civilite, nomCli, prenomCli, telephoneCli, dateNaisCli, adCli, villeCli, cpCli, mailCli, mdpCli FROM CLIENT WHERE numCli=?;');
            $reponse->execute(array($num));
            $donnees = $reponse->fetch();
            $unClient = new Client($donnees["numCli"], $donnees["civilite"], $donnees["nomCli"], $donnees["prenomCli"], $donnees["telephoneCli"], $donnees["dateNaisCli"], $donnees["adCli"], $donnees["villeCli"], $donnees["cpCli"], $donnees["mailCli"], $donnees["mdpCli"]);
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $unClient;
    }
}

class GestionPanier
{
    /**
    * Vérifie la présence d'un article dans le panier
    *
    * @param String $numProd référence du produit à vérifier
    * @return Boolean Renvoie Vrai si l'article est trouvé dans le panier, Faux sinon
    */
    /*
    public static function verifPanier($numProd)
    {
        /* On initialise la variable de retour */
        /*$isPresent = false;
        /* On vérifie les numéros de références des articles et on compare avec l'article à vérifier */
        /*if( count($_SESSION['panier']['numProduit']) > 0 && array_search($numProd,$_SESSION['panier']['numProduit']) !== false)
        {
            $isPresent = true;
        }
        return $isPresent;
    }
    */
    
    
    
    /**
    * Ajout d'un article dans le panier. Vérifie d'abord que l'article n'est pas déjà dans le panier.
    * Si l'article est absent, on l'ajoute.
    * S'il est présent, on met à jour en modifiant la quantité (y compris si c'est la même).
    *
    * @param array $numProduit variable tableau associatif contenant les valeurs de l'article
    */ 
    public static function ajoutProduit($numProduit)
    {
        
        /* On initialise la variable de retour (verifPanier) */
        $isPresent = false;
        
        if(array_key_exists($numProduit,$_SESSION['panier']))
        {
            $isPresent = true;
        }
        
        if(!$isPresent) { // s'il n'existe pas on l'ajoute
            $_SESSION['panier'][$numProduit]=1;
        } else { // sinon on modifie juste la quantité
            $_SESSION['panier'][$numProduit]++;
        }
    } 
    
    
    /**
    * Modifie la quantité d'un article dans le panier
    *
    * @param String $numProd        Identifiant de l'article à modifier
    * @param Int $qte               Nouvelle quantité à enregistrer
    * @return Mixed                 Retourne VRAI si la modification a bien eu lieu,
    *                               FAUX sinon,
    *                               "absent" si l'article est absent du panier,
    *                               "qte_ok" si la quantité n'est pas modifiée car déjà correctement enregistrée.
    */
    function modifierQte($numProd, $qte)
    {
        $_SESSION['panier'][$numProd] = $qte;
    } 
   
    
    
    /**
    * Supprimer un article du panier
    *
    * @param String     $numProd numéro de référence de l'article à supprimer
    * @return Mixed     Retourne TRUE si la suppression a bien été effectuée,
    *                   FALSE sinon, "absent" si l'article était déjà retiré du panier
    */
    public static function supprimerProduit($numProd)
    {
        unset($_SESSION['panier'][$numProd]);
    }
}

class GestionCommande
{

    public static function getLesCommandes()
    {
        require("connectBDD.php");
        try {
            $lesCommandes = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->query('SELECT * FROM COMMANDE');
            while ($donnees = $reponse->fetch()) {
                $lesCommandes[] = new Commande($donnees["numCli"], $donnees["numArt"], $donnees["quantite"]);
            }
            $reponse->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesCommandes;
    }

    public static function getCommandesParClient($numC)
    {
        require("connectBDD.php");
        try {
            $lesCommandes = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numArt, quantite FROM COMMANDE WHERE numCli=?');
            $reponse->execute(array($numC));
            while ($donnees = $reponse->fetch()) {
                $lesCommandes[] = new Commande($donnees["numCli"], $donnees["numArt"], $donnees["quantite"]);
            }
            $reponse->closeCursor();
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesCommandes;
    }
    
    public static function getCommandeParArt($numA)
    {
        require("connectBDD.php");
        try {
            $lesCommandes = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numCli, quantite FROM COMMANDE WHERE numArt=?');
            $reponse->execute(array($numA));
            while ($donnees = $reponse->fetch()) {
                $lesCommandes[] = new Commande($donnees["numCli"], $donnees["numArt"], $donnees["quantite"]);
            }
            $reponse->closeCursor();
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesCommandes;
    }
}
