<?php

require_once("Compte.class.php");

class GestionAdmin {

    public static function Connexion($unMail, $unMdp)
    {
        require("connectBDD.php");
        $admin = null;
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $reponse = $bdd->prepare('SELECT numCli, prenomCli, mdpCli FROM CLIENT WHERE mailCli=? AND isAdmin=1');
            $reponse->execute(array($unMail));
            $donnees = $reponse->fetch();
            if ($donnees) {
                $verifMdp = password_verify($unMdp, $donnees['mdpCli']);
                if ($verifMdp) {
                    $admin = new Compte($donnees['numCli'], $donnees['prenomCli'], $donnees['mdpCli']);
                }
            }
            $reponse->closeCursor();  // ferme le curseur

            return $admin;
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function GetAdmin($id)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('SELECT num, pseudo, mdp FROM ADMIN WHERE num = ?');
            $req->execute(array($id));
            $donnees = $req->fetch();
            $unAdmin = new Admin($donnees['num'], utf8_encode($donnees["pseudo"]), utf8_encode($donnees["mdp"]));
            $req->closeCursor();  // ferme le curseur
            
            return $unAdmin;
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    
    public static function getLesAdmins()
    {
        require("connectBDD.php");
        try {
            $lesAdmins = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->query('SELECT * FROM ADMIN');
            while ($donnees = $req->fetch()) {
                $lesAdmins[] = new Admin($donnees['num'], utf8_encode($donnees["pseudo"]), utf8_encode($donnees["mdp"]));
            }
            $req->closeCursor();  // ferme le curseur
            
            return $lesAdmins;
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function ModifierAdmin($id, $pseudo, $pass)
    {
        require("connectBDD.php");
        try {
            if (!$pass == '') {
                $pass_hache = password_hash($pass, PASSWORD_DEFAULT);
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
                $req = $bdd->prepare('UPDATE ADMIN SET pseudo=?, mdp=? WHERE num=?');
                $req->execute(array($pseudo, $pass_hache, $id));
                //echo "Le compte administrateur a été modifié !";
            } else {
                $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
                $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
                $req = $bdd->prepare('UPDATE ADMIN SET pseudo=? WHERE num=?');
                $req->execute(array($pseudo, $id));
                //echo "Le compte administrateur a été modifié !";
            }
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function SupprimerAdmin($id)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('DELETE FROM ADMIN WHERE num=?');
            $req->execute(array($id));
            //echo "Le compte administrateur a été supprimé !";
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
}
