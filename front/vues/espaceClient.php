<!--<h2>Mes informations</h2>

<?php /*echo $unCli->civilite; ?>
<br/>
<?php echo $unCli->nom; ?>
<br/>
<?php echo $unCli->prenom; ?>
<br/>
<?php echo $unCli->tel; ?>
<br/>
<?php echo $unCli->dateNais; ?>
<br/>
<?php echo $unCli->ad; ?>
<br/>
<?php echo $unCli->ville; ?>
<br/>
<?php echo $unCli->cp; ?>
<br/>
<?php echo $unCli->mail;*/ ?>
<br/>
<button onclick="window.location.href = 'index.php?page=modifierEspCli';">Modifier mon profil</button>
<button onclick="window.location.href = 'index.php?page=modifierCoCli';">Modifier mes informations</button>
-->
<h1>BIENVENUE !</h1>
<p><?php echo $unCli->prenom; ?> ☺</p>
<table class="table table-hover">
    <thead>
        <tr>
            <th scope="col">Civilité</th>
            <th scope="col">Nom</th>
            <th scope="col">Prénom</th>
            <th scope="col">Numéro de téléphone</th>
            <th scope="col">Date de naissance</th>
            <th scope="col">Adresse</th>
            <th scope="col">Ville</th>
            <th scope="col">Code Postal</th>
            <th scope="col">Adresse mail</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><?php echo $unCli->civilite; ?></td>
            <td><?php echo $unCli->nom; ?></td>
            <td><?php echo $unCli->prenom; ?></td>
            <td><?php echo $unCli->tel; ?></td>
            <td><?php echo $unCli->dateNais; ?></td>
            <td><?php echo $unCli->ad; ?></td>
            <td><?php echo $unCli->ville; ?></td>
            <td><?php echo $unCli->cp; ?></td>
            <td><?php echo $unCli->mail; ?></td>
        </tr>
    </tbody>
</table>

<button type="button" class="btn btn-danger" onclick="window.location.href = 'index.php?page=modifierEspCli';">Modifier mon profil</button>
<button type="button" class="btn btn-danger" onclick="window.location.href = 'index.php?page=modifierCoCli';">Modifier mes informations de connexion</button>
