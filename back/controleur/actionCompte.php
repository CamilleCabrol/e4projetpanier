<?php

include("../modeles/gestionCompte.class.php");

if (isset($_POST["action"])) {
    $action = $_POST["action"];
    switch ($action) {
        case 1:
            GestionCompte::AjouterUnCompte($_POST['civilite'], $_POST['nom'], $_POST['prenom'], $_POST['tel'], $_POST['dateNais'], $_POST['ad'], $_POST['ville'], $_POST['cp'], $_POST['mail'], $_POST['mdp']);
            break;

        case 2:
            GestionCompte::ModifierCompte($_POST['civilite'], $_POST['nom'], $_POST['prenom'], $_POST['tel'], $_POST['dateNais'], $_POST['ad'], $_POST['ville'], $_POST['cp'], $_POST['mail'], $_POST['mdp']);
            break;
	}
}
else {
    $delete = $_GET["delete"];
    switch ($delete) {
        case 3:
            GestionCompte::SupprimerUnCompte($_GET["num"]);
            break;
    }
}
header("Location: ../index.php?page=affichageCompte"); //affiche les occurrences après l'ajout
