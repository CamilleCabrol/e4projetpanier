<!--<footer class="page-footer font-small blue">
    <div class="footer-copyright text-center py-3">
        <?php /*echo "©" . date('Y');*/ ?> - Tous droits reservés - Camille CABROL.
    </div>
</footer>-->

<!--<footer class="page-footer">
    <div class="footer-copyright text-center py-3">
        Développé dans le cadre de mon BTS SIO.
        <br>
        <?php /*echo "©" . date('Y');*/ ?> - Tous droits reservés - Camille CABROL.
    </div>
</footer>-->
        
        
        
<!-- Footer -->
<footer class="page-footer font-small blue pt-4" style="background-color: #f8f9fa; margin-top: 50px;">
    <!-- Footer Links -->
    <div class="container-fluid text-center text-md-left" style="padding-left: 170px;">
        <!-- Grid row -->
        <div class="row">
            <!-- Grid column -->
            <div class="col-md-4 mt-md-0 mt-3">
                <!-- Content -->
                <h5 class="text-uppercase">BonbonTropBon</h5>
                <p>Nous nous excusons si vous ne pouvez pas passer à la validation de votre paiement et recevoir vos bonbons.
                Ce site est développé dans le cadre d'un projet informatique.</p>
            </div>
            <hr class="clearfix w-100 d-md-none pb-3">
            <!-- Grid column -->
            <div class="col-md-2 mb-md-0 mb-3">
                <!-- Links -->
                <h5 class="text-uppercase" >Mon compte</h5>
                <ul class="list-unstyled">
                    <li>
                        <a style="color: #dc3545;"  href="index.php?page=inscription">M'inscrire</a>
                    </li>
                    <li>
                        <a style="color: #dc3545;" href="index.php?page=connection">Me connecter</a>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
            <div class="col-md-2 mb-md-0 mb-3">
                <!-- Links -->
                <h5 class="text-uppercase">A propos</h5>
                <ul class="list-unstyled">
                    <li>
                        <a style="color: #dc3545;" href="index.php?page=listeProduit">Nos bonbons</a>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
            <div class="col-md-3 mb-md-0 mb-3">
                <!-- Links -->
                <h5 class="text-uppercase">Nous contacter</h5>
                <ul class="list-unstyled">
                    <li>
                        <a style="color: #dc3545;" href="index.php?page=listeProduit">Nos bonbons</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">
        <p>© 2020 Copyright - Tous droits reservés - Camille CABROL.</p>
    </div>
</footer>