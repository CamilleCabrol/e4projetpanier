<?php

if(session_status()== PHP_SESSION_NONE) {
    session_start();
}

include("../modeles/gestionAdmin.class.php");

if (isset($_POST["action"])) {
    $action = $_POST["action"];
    switch ($action) {

        case 2:
            $admin = GestionAdmin::Connexion($_POST['mail'], $_POST['mdp']);
            if (is_null($admin)) {
                header('Location: ../index.php?erreur=1');
            } else {
                $_SESSION['admin']=serialize($admin); 
                header('Location: ../index.php');
            }
            break;

        case 4:
            GestionAdmin::ModifierAdmin($_POST['num'], $_POST['pseudo'], $_POST['mdp']);
            header("Location: ../index.php?page=affichageInfoAdmin"); //affiche les occurrences après l'ajout
            break;
    }
} 
else 
{	
    $delete = $_GET["delete"];
    switch ($delete) {
        case 3:
            GestionAdmin::SupprimerAdmin($_GET['num']);
            header("Location: ../index.php?page=affichageInfoAdmin"); //affiche les occurrences après l'ajout
            break;
    }
}
