<?php

require_once("Categorie.class.php");

class GestionCategorie {
    
    public static function getLesCategories()
    {
        require("connectBDD.php");
        try {
            $lesCategs = array();
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->query('SELECT * FROM CATEGORIE');
            while ($donnees = $req->fetch()) {
                $lesCategs[] = new Categorie($donnees['numCateg'], $donnees["libCateg"]);
            }
            $req->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $lesCategs;
    }

    public static function getLaCategorie($id)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('SELECT * FROM CATEGORIE WHERE numCateg=?');
            $req->execute(array($id));
            $donnees = $req->fetch();
            $uneCateg = new Categorie($donnees['numCateg'], utf8_encode($donnees['libCateg']));
            $req->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
        return $uneCateg;
    }

    public static function AjouterUneCategorie($lib)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare("INSERT INTO CATEGORIE (libCateg) values (?)");
            $req->execute(array($lib));
            $req->closeCursor();  // ferme le curseur
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
    
    public static function ModifierUneCategorie($id, $lib)
    {
        require("connectBDD.php");
        try {
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('UPDATE CATEGORIE SET libCateg=? WHERE numCateg=?');
            $req->execute(array($lib, $id));
            $req->closeCursor();  // ferme le curseur
            //echo "La catégorie a été modifié !";
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }

    public static function SupprimerUneCategorie($id)
    {
        require("connectBDD.php");
        try {
            require_once("GestionCategorie.class.php");
            $pdo_options[PDO::ATTR_ERRMODE] = PDO::ERRMODE_EXCEPTION;
            $bdd = new PDO('mysql:host=' . $host . ';dbname=' . $bdd, $util, $password, $pdo_options);
            $req = $bdd->prepare('DELETE FROM CATEGORIE WHERE numCateg=?');
            $req->execute(array($id));
            $req->closeCursor();  // ferme le curseur
            //echo "La catégorie a été supprimé !";
        } catch (Exception $e) {
            die('Erreur : ' . $e->getMessage());
        }
    }
}
