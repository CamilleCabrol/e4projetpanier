<!--<nav>
    <ul class="nav">
        <li class="active">
            <a href="index.php">Accueil</a>
        </li>
        <li>
            <a href="index.php?page=affichageProduit">Les produits</a>
        </li>
        <li>
            <a href="index.php?page=affichageCategorie">Les catégories</a>
        </li>
        <li>
            <a href="index.php?page=deconnexion">Se déconnecter</a>
        </li>
    </ul>
</nav>-->

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" >BonbonTropBon</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-link active" href="index.php">Accueil<span class="sr-only">(current)</span></a>
            <a class="nav-link" href="index.php?page=affichageProduit">Les produits</a>
            <a class="nav-link" href="index.php?page=affichageCategorie">Les catégories</a>
            <a class="nav-link" href="index.php?action=deconnexion">Se déconnecter</a>
        </div>
    </div>
</nav>